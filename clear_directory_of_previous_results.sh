#!/bin/bash

# Remove the figure files
rm AnalysisResults/Figures/*/*.pdf

# Remove the text files
rm AnalysisResults/TextFiles/PolishedExcelFilesForPaper/*.xlsx # Remove POlished Excel files
rm AnalysisResults/TextFiles/RawAnalysisOutput/*.txt # Remove raw analysis output
rm AnalysisResults/TextFiles/SemicleanedExcelFiles/AnalysisCondensing_Template_Completed.xlsx # AnalysisCondensing

