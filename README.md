# A project to study differences between pediatric and adult CT imaging, in partnership with Imalogix.

#### contact: Taylor Smith, taylor.smith@duke.edu

### Background:
	There is considerable evidence that there is room to improve CT 
	imaging practice of pediatric patients by differentiating it 
	from adult patients. 
	
### Scope:
	This project considers a large Imalogix-client dataset
	to analyze:
		mAs, kVp, tube rotation time, and pitch, and their effects on 
		CTDIvol and SSDE in both pediatric and adult populations, for chest 
		and abdomen-pelvis exams
		
### Code:
	Is largely written in python to import data, filter and augment 
	the dataset to include the appropriate cohort (ages, protocols)
	and to analyze using a Kruskall-Wallis test and median comparisons, 
	and plot results, and to write results programmatically which can be used 
	for further analysis. Another Matlab script is provided which does further analysis
	of the number of facilities which are changing scan parameters in a consistent way, 
	and which of these are concurrent with effects of lowering the CTDIvol and SSDE.

### Dataset:
	"/home/tbs16/Data/Imalogix/pediatricRadiology/DatasetVersions/pedRadiology_v2.txt"

### Functions:
	"ImageGently_TaylorSmith_v2.ipynb" - Main analysis code written as a python Jupyter notebook. .
	    Imports the downloaded dataset, and runs analysis generating output, and images for review and reporting.
	    This is the bulk of the repository in terms of importance.
	    
	    Inputs are all in the first 'code' cell of the notebook and are described there.
	    For simplicity they are described briefly here as well as:
	        "showIQ" = False: Whether or not to show the Image Quality features on the plot. 
	                  Leave this False
	                  Default = False
            "percentilReported" = 0.5: What is the reported percentile of the 
                                 parameter histograms
                                 
            "protoType" = 'Abd-Pelvis' or, 'Chest': Determines which of the type 
                           of protocols the analysis will show for.
                                  
            "minNumDataPoints" = 11: For this anaylsis N>11 is used, but in principle 
                                     anything can be selected
           
	    
	"ConsistencyResultAnalysis.m" - Auxiliary analysis code written in Matlab. 
	    Takes "Analysis_Result_v2.xlsx" as an input, and generates a Matlab structure 
	    which describes the results of how many facilities make changes to their 
	    scan parameters "consistently" (that is, across all pediatric cohorts). 
	    This is one of the main things that is reported as the result of the paper. 
	    It is run after the "ImageGently_TaylorSmith_v2.ipynb" created output.
	    
	 "processResults.ipynb" - Auxiliary analysis code written in python. 
	    Takes output from "ImageGently_TaylorSmith_v2.ipynb" as an input, 
	    and generates text files and figures which describe the result of the analysis 
	    including 
	        a. How many facilities make changes to their scan parameters "consistently" (that is, across all pediatric cohorts). 
	        b. Differences in CTDIvol and SSDE in pediatric and adult cohorts
	        c. What protocols are included in the analysis
	    It is run only after the "ImageGently_TaylorSmith_v2.ipynb" created output.
	    	    
	
### Run Instructions:
	1.)	Download dataset: "pedRadiology_v2.txt" and 
	    put it in a safe directory where it will not be changed or altered such as 
	    {"imalogix_pediatricctimaging/Dataset/pedRadiology_v2.txt"} 
	    in this work. 
	
	2.) Navigate to and set permissions on the directory so that the docker container can access the files 
	    (if necessary, as in running on a server or other machine) with: 
	    $ chmod -R 777 imalogix_pediatricctimaging/ 
	        or by identifying another user

	3.)	Run python notebook in docker container using either:
	    if on local machine:
			docker run -p 8888:8888 -it -v "$PWD":/home/jovyan/ jupyter/scipy-notebook:7a0c7325e470 bash
			or
		if on remotely on server:
		    docker run --net=host -p 8888:8888 -it -v "$PWD":/home/jovyan/ jupyter/scipy-notebook:7a0c7325e470 bash

	4.)	In the running docker container, open jupyter notebook with the command:
			$ jupyter notebook
			and follow the directions at the prompt to open the notebook 
			(if running remotely, you may need to change the ip address before the port ID to reflect the server)
	        like, changing http://203.0.113.11:8888/?token=sometokenstring, to http://me@my.server.name.com:8888/?token=sometokenstring

		Run python notebook "ImageGently_TaylorSmith_v2.ipynb" to produce output in 
	    AnalysisResults subdirectories "Figures" and "TextFiles"
			Figures - five subdirectories "histogramParameters"
				                         
				"histogramParameters" - gives histograms for the parameters (kVp, mAs, pitch, rotation time) 
				                        and dose (CTDIvol, side) for each of the locations for each of the 
				                        pediatric age groups. (Used as figure in paper)
				                        
			
			TextResults - three subdirectories "PolishedExcelFilesForPaper", "RawAnalysisOutput",  "SemicleanedExcelFiles"
				"RawAnalysisOutput" - the raw output of the analysis. This will be a .txt file and 
				                      should be imported into excel using the '|' as a delimiter. 
				                      This is done manually, and saved as a .xlsx file in "SemicleanedExcelFiles."
				                      
				"SemicleanedExcelFiles" - hold the output from RawAnalysisOutput in the form of an excel file. 
				                          Currently the method of creating the polished versions is to take 
				                          these and make them replicate the template held in "AnalysisCondensing_Template.xlsx"
				                          which we will save as "Analysis__CohortComparison-v2__percentileReported-{50}__minNumDataPoints-{11}__whichPhase-{1}__reqNoMissingData-{True}__{Abd-Pelvis}_Protocols.xlsx" 
				                          (where all {} denote parameters which could be replaced where necessary, 
				                          but denote the standard settings for this analysis). Then we take these 
				                          and make them slightly more polished for presenting in the analysis. 
				                          Currently this is also done manually but should be replaced by a programmatic method.
				                          
    
    4.)     Once 3 is completed, run the python notebook "ProcessResults.ipynb" to to generate results of how often
	        facilities made consistent changes to the kVp, mAs, pitch, and rotation time, as well as how often 
	        these impacted the CTDIvol and the SSDE.
            These will produce output in AnalysisResults subdirectories "Figures" and "TextFiles"
            In the subdirectories "changedParameters", "ctdiVolHistoColumns", "representedProtocols", "ssdeHistoColumns"
            
            	"representedProtocols" - gives bar chart of which protocols are included 
				                         in the analysis and their frequency (Used as figure in the paper)

	            "changedParameters" - gives bar charts of the fraction of cohorts for which each parameter 
				                      is changed (KW p < 0.05, median shifted appropriately), conditioned on 
				                      each pediatric age group. (Used with ctdiVolHistoColumns and ssdeHistoColumns 
				                      to make a figure in the paper)
				                      
				"ctdiVolHistoColumns" - gives ctdivol histograms conditioned on each pediatric age group and 
				                        compared to the adult age group. (Used with changedParameters and 
				                        ssdeHistoColumns to make a figure in the paper)
				                        
				"ssdeHistoColumns" - gives ssde histograms conditioned on each pediatric age group and
				                     compared to the adult age group. (Used with changedParameters and 
				                     ctdivolHistoColumns to make a figure in the paper)
				                     
				"PolishedExcelFilesForPaper" - the final product of the analysis is a table which will be presentable in the appendix of a paper. 
				                               

	
