clear all;
close all;
clc;

% How many parameters
nParams = 6;

% Get the file name
filename = '/Users/taylorsmith/Documents/Python/ImalogixProject/SmallImalogixProject/pedRadiology_ImageGently/Analysis_Results.xlsx';
filename = '/Users/taylorsmith/Documents/Python/ImalogixProject/SmallImalogixProject/pedRadiology_ImageGently/Analysis_Result_v2.xlsx';
filename = '/Users/taylorsmith/Documents/Python/ImalogixProject/SmallImalogixProject/pedRadiology_ImageGently/AnalysisResults/TextFiles/SemicleanedExcelFiles/AnalysisCondensing_Template_Completed.xlsx';

% Choose the protocol
protos = {'Abd-Pelvis','Chest'};

for j =1: length(protos)
    proto = protos{j};
    
% Load the data
[~,sheet_name]=xlsfinfo(filename);
for k=1:numel(sheet_name)
  data{k}=xlsread(filename,sheet_name{k});
  dataTab{k}=readtable(filename,'Sheet',sheet_name{k});
  if(strcmp(sheet_name{k},'Final_Abd-Pelvis'))
      dataTab_AbdPelv = dataTab{k};
  elseif(strcmp(sheet_name{k},'Final_Chest'))
      dataTab_Chest = dataTab{k};
  end
end

if(strcmp(proto,'Abd-Pelvis'))
    dat = dataTab_AbdPelv;
end
if(strcmp(proto,'Chest'))
    dat = dataTab_Chest;
end

% Reset for the rounf
consts = [];
counts = [];
% Get the locations
locs = unique(dat{:,1});
for i = 1:length(locs)
    
    %
    theseidx = dat{:,1} == locs(i);
    thisdat = dat(theseidx,:);
    
    % Get how many of these are consistent
    ncohorts = sum(theseidx);
    thisloc = locs(i);
    kvconst = sum(thisdat{:,4}) == ncohorts;
    maconst = sum(thisdat{:,5}) == ncohorts;
    pitchconst = sum(thisdat{:,6}) == ncohorts;
    revtimeconst = sum(thisdat{:,7}) == ncohorts;
    ctdiconst = sum(thisdat{:,8}) == ncohorts;
    ssdeconst = sum(thisdat{:,9}) == ncohorts;
    
    consts(i,1) = thisloc;
    consts(i,2) = kvconst;
    consts(i,3) = maconst;
    consts(i,4) = pitchconst;
    consts(i,5) = revtimeconst;
    consts(i,6) = ctdiconst;
    consts(i,7) = ssdeconst;
    consts(i,8) = ncohorts;
    
    % Count how many of these cohorts have a change
    kvcounts = sum(thisdat{:,4});
    macounts = sum(thisdat{:,5});
    pitchcounts = sum(thisdat{:,6});
    revtimecounts = sum(thisdat{:,7});
    ctdicounts = sum(thisdat{:,8});
    ssdecounts = sum(thisdat{:,9});
 
    counts(i,1) = thisloc;
    counts(i,2) = kvcounts;
    counts(i,3) = macounts;
    counts(i,4) = pitchcounts;
    counts(i,5) = revtimecounts;
    counts(i,6) = ctdicounts;
    counts(i,7) = ssdecounts;
    counts(i,8) = ncohorts;
    
end

% Write the count and consistency tables
if(strcmp(proto,'Chest'))
    endConsistencyTable_chest = array2table(consts,'VariableNames',{'Location','Consistent__kVp','Consistent__mAs','Consistent__Pitch','Consistent__Rot_Time','Consistent__CTDIvol','Consistent__SSDE','N_Ped_Cohorts'});
    endCountsTable_chest = array2table(counts,'VariableNames',{'Location','Counts__kVp','Counts__mAs','Counts__Pitch','Counts__Rot_Time','Counts__CTDIvol','Counts__SSDE','N_Ped_Cohorts'});
end

if(strcmp(proto,'Abd-Pelvis'))
    endConsistencyTable_abd = array2table(consts,'VariableNames',{'Location','Consistent__kVp','Consistent__mAs','Consistent__Pitch','Consistent__Rot_Time','Consistent__CTDIvol','Consistent__SSDE','N_Ped_Cohorts'});
    endCountsTable_abd = array2table(counts,'VariableNames',{'Location','Counts__kVp','Counts__mAs','Counts__Pitch','Counts__Rot_Time','Counts__CTDIvol','Counts__SSDE','N_Ped_Cohorts'});
end  

end

%% Calculate the fParameterJ
Stats.Chest.fParameter_j.kVp = mean(dataTab_Chest.kVp);
Stats.AbdPelv.fParameter_j.kVp = mean(dataTab_AbdPelv.kVp);

Stats.Chest.fParameter_j.mAs = mean(dataTab_Chest.mAs);
Stats.AbdPelv.fParameter_j.mAs = mean(dataTab_AbdPelv.mAs);

Stats.Chest.fParameter_j.Pitch = mean(dataTab_Chest.Pitch);
Stats.AbdPelv.fParameter_j.Pitch = mean(dataTab_AbdPelv.Pitch);

Stats.Chest.fParameter_j.Rot_Time = mean(dataTab_Chest.Rot_Time);
Stats.AbdPelv.fParameter_j.Rot_Time = mean(dataTab_AbdPelv.Rot_Time);

Stats.Chest.fParameter_j.ctdi = mean(dataTab_Chest.CTDIvol);
Stats.AbdPelv.fParameter_j.ctdi = mean(dataTab_AbdPelv.CTDIvol);

Stats.Chest.fParameter_j.ssde = mean(dataTab_Chest.SSDE);
Stats.AbdPelv.fParameter_j.ssde = mean(dataTab_AbdPelv.SSDE);

%% Calculate the fFacilityI
tmpChest = array2table(((endCountsTable_chest.Counts__kVp + endCountsTable_chest.Counts__mAs + endCountsTable_chest.Counts__Pitch + endCountsTable_chest.Counts__Rot_Time + endCountsTable_chest.Counts__CTDIvol + endCountsTable_chest.Counts__SSDE) ./ (nParams * endCountsTable_chest.N_Ped_Cohorts)),'VariableNames',{'fFacility_i_chest'});
Stats.Chest.fFacility_i = [endConsistencyTable_chest(:,1), tmpChest];

tmpAbdPelv = array2table(((endCountsTable_abd.Counts__kVp + endCountsTable_abd.Counts__mAs + endCountsTable_abd.Counts__Pitch + endCountsTable_abd.Counts__Rot_Time + endCountsTable_abd.Counts__CTDIvol + endCountsTable_abd.Counts__SSDE) ./ (nParams * endCountsTable_abd.N_Ped_Cohorts)),'VariableNames',{'fFacility_i_abd'});
Stats.AbdPelv.fFacility_i = [endConsistencyTable_abd(:,1), tmpAbdPelv];


%% Get how many cohorts had a parameter changed. Then take these places and see how many of the
whatIDXChanged_Chest = (dataTab_Chest.kVp+dataTab_Chest.mAs+dataTab_Chest.Pitch+dataTab_Chest.Rot_Time)>0;
whatIDXChanged_AbdPelv = (dataTab_AbdPelv.kVp+dataTab_AbdPelv.mAs+dataTab_AbdPelv.Pitch+dataTab_AbdPelv.Rot_Time)>0;

Stats.Chest.changed.CTDI = mean(dataTab_Chest.CTDIvol(whatIDXChanged_Chest));
Stats.Chest.changed.SSDE = mean(dataTab_Chest.SSDE(whatIDXChanged_Chest));
Stats.Chest.notchanged.CTDI = mean(dataTab_Chest.CTDIvol(~whatIDXChanged_Chest));
Stats.Chest.notchanged.SSDE = mean(dataTab_Chest.SSDE(~whatIDXChanged_Chest));

Stats.Chest.nCohorts.total = size(whatIDXChanged_Chest,1);
Stats.Chest.nCohorts.changed = sum(whatIDXChanged_Chest);
Stats.Chest.nCohorts.notchanged = sum(~whatIDXChanged_Chest);
clear whatIDXChanged_Chest

Stats.AbdPelv.changed.CTDI = mean(dataTab_AbdPelv.CTDIvol(whatIDXChanged_AbdPelv));
Stats.AbdPelv.changed.SSDE = mean(dataTab_AbdPelv.SSDE(whatIDXChanged_AbdPelv));
Stats.AbdPelv.notchanged.CTDI = mean(dataTab_AbdPelv.CTDIvol(~whatIDXChanged_AbdPelv));
Stats.AbdPelv.notchanged.SSDE = mean(dataTab_AbdPelv.SSDE(~whatIDXChanged_AbdPelv));

Stats.AbdPelv.nCohorts.total = size(whatIDXChanged_AbdPelv,1);
Stats.AbdPelv.nCohorts.changed = sum(whatIDXChanged_AbdPelv);
Stats.AbdPelv.nCohorts.notchanged = sum(~whatIDXChanged_AbdPelv);
clear whatIDXChanged_AbdPelv

%%
% Save the files into some kind of output and then
save('AnalysisResults/TextFiles/ConsistencyResults/results_analysis_Matlab.mat','endConsistencyTable_chest', 'endConsistencyTable_abd', 'dataTab_AbdPelv', 'dataTab_Chest','Stats');

clear all;
load('AnalysisResults/TextFiles/ConsistencyResults/results_analysis_Matlab');